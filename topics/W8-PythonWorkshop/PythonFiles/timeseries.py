# -*- coding: utf-8 -*-

import pandas as pd
from matplotlib import pyplot as plt

market_data = pd.read_csv('stock/stock_close.csv', parse_dates=True, 
                          index_col=0)

#market_data = market_data[['MSFT','RHT','GOOGL']]
#market_data = market_data.resample('D').ffill()

fig, axes = plt.subplots(1,1,sharex=True,sharey=True, figsize=(7,7))

market_data['MSFT'].plot(ax=axes)
market_data['GOOGL'].plot(ax=axes)
market_data['RHT'].plot(ax=axes)

axes.set_title("Closing Prices")
axes.legend(loc='best')
axes.get_figure().savefig('stocks.png') # saves the figure as png

# -*- coding: utf-8 -*-

import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

points = np.arange(-5,5, 0.1)

x, y = np.meshgrid(points,points)

z = np.sinc( np.sqrt( x**2 + y**2))

fig = plt.figure()
ax = Axes3D(fig)
ax.plot_surface(x,y,z,rstride=1, cstride=1, linewidth=0 ,cmap='coolwarm')
ax.set_title("Plot of $\sin(\sqrt{x^2+y^2})/\sqrt{x^2+y^2}$")
